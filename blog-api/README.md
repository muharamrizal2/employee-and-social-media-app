# Social Media APP
## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation Service

```bash
$ yarn install 
```
on blog-api

## ## Installation Database
- Make sure docker and docker compose has been instaled on your device
- Change directory into database
- open your terminal and run ``` docker-compose up -d``` to run docker on the background
- ```chmod +x create_index.sh ```  on terminal on directory dataset
- ```./create_index.sh       ``` execute on terminal (to run the bash script)

## Running the app
# watch mode
$ yarn start dev

make sure your directory on blog-api

## Test
test not finished setup

## Read The API END POINT SWAGGER
LINK[https://documenter.getpostman.com/view/6168502/2sA3XMiNfz]
