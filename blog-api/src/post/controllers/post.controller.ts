import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  Put,
  Delete,
  Req,
  Inject,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/auth.guard';
import { POST_SERVICE } from '../post.constant';
import { PostService } from '../services/post.service';

@Controller('posts')
export class PostController {
  constructor(
    @Inject(POST_SERVICE) private readonly postsService: PostService,
  ) {}

  @Get()
  getAllPosts() {
    return this.postsService.findAll();
  }

  @Get(':id')
  getPostById(@Param('id') id: string) {
    return this.postsService.findOne(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  createPost(@Body() createPostDto, @Req() req) {
    return this.postsService.create(createPostDto, req.user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  updatePost(@Param('id') id: string, @Body() updatePostDto, @Req() req) {
    return this.postsService.update(id, updatePostDto, req.user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  deletePost(@Param('id') id: string, @Req() req) {
    return this.postsService.remove(id, req.user.id);
  }
}
