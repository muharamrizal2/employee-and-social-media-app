import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { ErrorObject, ErrorResponse } from 'src/exceptions/models/error-object';
import { Post } from '../entities/post.entity';

@Injectable()
export class PostRepository {
  constructor(
    @InjectModel(Post)
    private postModel: typeof Post,
  ) {}

  async createPost(createPostDto: any, authorId: string): Promise<Post> {
    return this.postModel.create({ ...createPostDto, authorId });
  }

  async findAllPosts(): Promise<Post[]> {
    return this.postModel.findAll();
  }

  async findPostById(id: string): Promise<Post> {
    return this.postModel.findOne({ where: { id } });
  }

  private errorUnauthorized() {
    const newErr = new ErrorObject();
    const errorResp = new ErrorResponse();
    errorResp.errors = [];
    (newErr.title = 'Action Failed'), (newErr.detail = `Post unauthorized`);
    errorResp.errors.push(newErr);

    throw new BadRequestException(errorResp);
  }

  async updatePost(
    id: string,
    updatePostDto: any,
    authorId: string,
  ): Promise<Post> {
    const post = await this.findPostById(id);
    if (!post) {
      const newErr = new ErrorObject();
      const errorResp = new ErrorResponse();
      errorResp.errors = [];
      (newErr.title = 'Update Failed'), (newErr.detail = `Post not found`);
      errorResp.errors.push(newErr);

      throw new BadRequestException(errorResp);
    }

    if (post.authorId !== authorId) {
      this.errorUnauthorized();
    }
    await post.update(updatePostDto);
    return post;
  }

  async deletePost(id: string, authorId: string): Promise<void> {
    const post = await this.findPostById(id);
    if (!post || post.authorId !== authorId) {
      const newErr = new ErrorObject();
      const errorResp = new ErrorResponse();
      errorResp.errors = [];
      (newErr.title = 'Delete Failed'),
        (newErr.detail = `Post not found or unauthorized`);
      errorResp.errors.push(newErr);

      throw new BadRequestException(errorResp);
    }
    await post.destroy();
  }
}
