import {
  Injectable,
  NotFoundException,
  ForbiddenException,
  Inject,
} from '@nestjs/common';
import { ErrorObject, ErrorResponse } from 'src/exceptions/models/error-object';
import { PostDto } from '../dto/post.dto';
import { Post } from '../entities/post.entity';
import { POST_REPO } from '../post.constant';
import { PostRepository } from '../repositories/post.repository';

@Injectable()
export class PostService {
  constructor(
    @Inject(POST_REPO) private readonly postRepository: PostRepository,
  ) {}

  async findAll(): Promise<Post[]> {
    return this.postRepository.findAllPosts();
  }

  async findOne(id: string): Promise<Post> {
    const post = await this.postRepository.findPostById(id);
    if (!post) {
      const newErr = new ErrorObject();
      const errorResp = new ErrorResponse();
      errorResp.errors = [];
      (newErr.title = 'Post Not Found'), (newErr.detail = `Post Not Found`);
      errorResp.errors.push(newErr);
      throw new NotFoundException(errorResp);
    }
    return post;
  }

  async create(postDto: PostDto, userId: string): Promise<Post> {
    return this.postRepository.createPost(postDto, userId);
  }

  async update(id: string, postDto: PostDto, userId: string): Promise<Post> {
    const post = await this.postRepository.updatePost(id, postDto, userId);
    if (!post) {
      const newErr = new ErrorObject();
      const errorResp = new ErrorResponse();
      errorResp.errors = [];
      (newErr.title = 'Update Error'),
        (newErr.detail = `You do not have permission to edit this post`);
      errorResp.errors.push(newErr);
      throw new ForbiddenException(errorResp);
    }
    return post;
  }

  async remove(id: string, userId: string): Promise<void> {
    await this.postRepository.deletePost(id, userId);
  }
}
