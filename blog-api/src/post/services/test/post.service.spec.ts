import { Test, TestingModule } from '@nestjs/testing';
import { NotFoundException, ForbiddenException } from '@nestjs/common';
import { PostService } from '../post.service';
import { PostRepository } from 'src/post/repositories/post.repository';
import { Post } from 'src/post/entities/post.entity';
import { PostDto } from 'src/post/dto/post.dto';

describe('PostService', () => {
  let service: PostService;
  let repository: PostRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostService,
        {
          provide: PostRepository,
          useValue: {
            findAllPosts: jest.fn(),
            findPostById: jest.fn(),
            createPost: jest.fn(),
            updatePost: jest.fn(),
            deletePost: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<PostService>(PostService);
    repository = module.get<PostRepository>(PostRepository);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should find all posts', async () => {
    const posts = [new Post()];
    repository.findAllPosts = jest.fn().mockResolvedValue(posts);
    expect(await service.findAll()).toEqual(posts);
  });

  it('should find one post by ID', async () => {
    const post = new Post();
    repository.findPostById = jest.fn().mockResolvedValue(post);
    expect(await service.findOne('1')).toEqual(post);
  });

  it('should throw NotFoundException if post not found', async () => {
    repository.findPostById = jest.fn().mockResolvedValue(null);
    await expect(service.findOne('1')).rejects.toThrow(NotFoundException);
  });

  it('should create a post', async () => {
    const postDto: PostDto = { content: 'Content' };
    const post = new Post();
    repository.createPost = jest.fn().mockResolvedValue(post);
    expect(await service.create(postDto, '1')).toEqual(post);
  });

  it('should update a post', async () => {
    const postDto: PostDto = {
      content: 'Updated Content',
    };
    const post = new Post();
    repository.updatePost = jest.fn().mockResolvedValue(post);
    expect(await service.update('1', postDto, '1')).toEqual(post);
  });

  it('should throw ForbiddenException if update unauthorized', async () => {
    repository.updatePost = jest.fn().mockResolvedValue(null);
    await expect(service.update('1', { content: '' }, '1')).rejects.toThrow(
      ForbiddenException,
    );
  });

  it('should delete a post', async () => {
    repository.deletePost = jest.fn().mockResolvedValue(undefined);
    await expect(service.remove('1', '1')).resolves.toBeUndefined();
  });
});
