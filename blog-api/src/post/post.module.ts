import { Module, Provider } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { UserModule } from 'src/user/user.module';
import { PostController } from './controllers/post.controller';
import { Post } from './entities/post.entity';
import { POST_REPO, POST_SERVICE } from './post.constant';
import { PostRepository } from './repositories/post.repository';
import { PostService } from './services/post.service';

const providers: Provider[] = [
  {
    provide: POST_SERVICE,
    useClass: PostService,
  },
  {
    provide: POST_REPO,
    useClass: PostRepository,
  },
];

@Module({
  imports: [SequelizeModule.forFeature([Post]), UserModule],
  providers: [...providers],
  controllers: [PostController],
})
export class PostModule {}
