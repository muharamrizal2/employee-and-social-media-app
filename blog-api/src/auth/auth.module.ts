import { forwardRef, Module, Provider } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UserModule } from 'src/user/user.module';
import { AuthService } from './service/auth.service';
import { JwtStrategy } from './auth.strategy';
import { AUTH_SERVICE, JWT_STRATEGY } from './auth.constant';
const providers: Provider[] = [
  {
    provide: JWT_STRATEGY,
    useClass: JwtStrategy,
  },
  {
    provide: AUTH_SERVICE,
    useClass: AuthService,
  },
];

@Module({
  imports: [
    forwardRef(() => UserModule),
    PassportModule,
    JwtModule.register({
      secret: 'keepmitsecret',
      signOptions: { expiresIn: '60m' },
    }),
  ],
  providers: [...providers],
  exports: [...providers],
})
export class AuthModule {}
