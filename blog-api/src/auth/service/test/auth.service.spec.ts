import { Test, TestingModule } from '@nestjs/testing';
import { JwtService } from '@nestjs/jwt';
import { UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../auth.service';
import { UserService } from 'src/user/services/user.service';
import { User } from 'src/user/entities/user.entity';
import * as bcrypt from 'bcrypt';

describe('AuthService', () => {
  let service: AuthService;
  let userService: UserService;
  let jwtService: JwtService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UserService,
          useValue: {
            findOneByEmail: jest.fn(),
          },
        },
        {
          provide: JwtService,
          useValue: {
            sign: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    userService = module.get<UserService>(UserService);
    jwtService = module.get<JwtService>(JwtService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should validate user', async () => {
    const user = new User();
    user.email = 'test@example.com';
    user.password = 'hashedpassword';

    userService.findOneByEmail = jest.fn().mockResolvedValue(user);
    jest.spyOn(bcrypt, 'compare').mockResolvedValue(true);

    expect(await service.validateUser('test@example.com', 'password')).toEqual(
      user,
    );
  });

  it('should throw UnauthorizedException for invalid credentials', async () => {
    userService.findOneByEmail = jest.fn().mockResolvedValue(null);
    await expect(
      service.validateUser('test@example.com', 'password'),
    ).rejects.toThrow(UnauthorizedException);
  });

  it('should generate JWT token for login', async () => {
    const user = new User();
    user.id = '1';
    user.email = 'test@example.com';

    jwtService.sign = jest.fn().mockReturnValue('token');

    expect(await service.login(user)).toEqual({ access_token: 'token' });
  });
});
