module.exports = {
  development: {
    username: 'bloguser',
    password: 'blogpassword',
    database: 'blog',
    host: 'localhost',
    dialect: 'mysql',
  },
};
