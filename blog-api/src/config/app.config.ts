export interface AppConfig {
  jwtSecret: string;
  jwtExpiresIn: string;
}

export default () => ({
  app: {
    jwtSecret: process.env.APP_JWT_SECRET,
    jwtExpiresIn: process.env.APP_JWT_EXPIRES_IN,
  },
});
