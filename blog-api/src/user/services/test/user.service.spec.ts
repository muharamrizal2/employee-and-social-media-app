// import { Test, TestingModule } from '@nestjs/testing';
import { ConflictException } from '@nestjs/common';
import { UserService } from '../user.service';
// import { UserRepository } from '../../repositories/user.repository';
import { CreateUserDto } from '../../dto/create-user.dto';
// import { mockUserRepository } from 'src/user/repositories/_mocks_/user.repository';
// import { USER_REPO } from 'src/user/user.constant';
// import { User } from 'src/user/entities/user.entity';
// import { getModelToken } from '@nestjs/sequelize';
import { mock, mockClear } from 'jest-mock-extended';
import { User } from 'src/user/entities/user.entity';
import { IUserRepository } from 'src/user/repositories/interface/user.repository.interface';
import { UserRepository } from 'src/user/repositories/user.repository';

describe('UserService', () => {
  // let userService: UserService;
  // let userRepository: typeof mockUserRepository; 
  // const mockUserModel = () => ({
  //   createUser: jest.fn().mockResolvedValue({} as User),
  //   findOneByEmail: jest.fn().mockResolvedValue({} as User),
  //   findById: jest.fn().mockResolvedValue({} as User),
  //   updateUser: jest.fn().mockResolvedValue({} as User),
  //   deleteUser: jest.fn().mockResolvedValue(undefined),
  // });

  // beforeEach(async () => {
  //   const module: TestingModule = await Test.createTestingModule({
  //     providers: [
  //       UserService,
  //       {
  //         provide: getModelToken(User),
  //         useFactory: mockUserModel,
  //       },
  //     ],
  //   }).compile();
  //   userService = new UserService(userRepository);

  //   userService = module.get<UserService>(UserService);
  //   userRepository = module.get<typeof mockUserRepository>(USER_REPO);
  // });

  let userService: UserService;
  const userRepo = mock<IUserRepository>() as unknown as UserRepository;

  beforeEach(() => {
    userService = new UserService(userRepo);
  });

  afterEach(() => {
    mockClear(userRepo);
  });

  it('should be defined', () => {
    expect(userService).toBeDefined();
  });

  it('should create a new user', async () => {
    const createUserDto: CreateUserDto = {
      name: 'test',
      email: 'test@example.com',
      password: 'password123',
    };

    const user = new User({
      name: 'test',
      email: 'test@example.com',
      password: 'password123',
    });
    userRepo.findOneByEmail.mockResolvedValue(null);
    userRepo.createUser.mockResolvedValue(user);

    expect(await userService.create(createUserDto)).toEqual(user);
  });

  it('should throw a ConflictException if email already exists', async () => {
    const createUserDto: CreateUserDto = {
      name: 'test',
      email: 'test@example.com',
      password: 'password123',
    };

    const user = new User({
      name: 'test',
      email: 'test@example.com',
      password: 'password123',
    });

    userRepo.findOneByEmail.mockResolvedValue(user);

    await expect(userService.create(createUserDto)).rejects.toThrow(
      ConflictException,
    );
  });
});
