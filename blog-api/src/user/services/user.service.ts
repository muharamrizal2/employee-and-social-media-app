// src/users/users.service.ts

import { ConflictException, Inject, Injectable } from '@nestjs/common';
import { CreateUserDto } from '../dto/create-user.dto';
import { User } from '../entities/user.entity';
import { UserRepository } from '../repositories/user.repository';
import { USER_REPO } from '../user.constant';

@Injectable()
export class UserService {
  constructor(
    @Inject(USER_REPO) private readonly userRepository: UserRepository,
  ) {}

  async create(user: CreateUserDto): Promise<User> {
    const existingUser = await this.userRepository.findOneByEmail(user.email);
    if (existingUser) {
      throw new ConflictException('Email already exists');
    }
    return this.userRepository.createUser({ ...user });
  }

  async findOneByEmail(email: string): Promise<User | undefined> {
    return this.userRepository.findOneByEmail(email);
  }

  async findOneById(id: string): Promise<User | undefined> {
    return this.userRepository.findById(id);
  }
}
