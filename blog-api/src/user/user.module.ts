import { forwardRef, Module } from '@nestjs/common';
import { Provider } from '@nestjs/common/interfaces';
import { SequelizeModule } from '@nestjs/sequelize';
import { AuthModule } from 'src/auth/auth.module';
import { UserController } from './controllers/user.controller';
import { User } from './entities/user.entity';
import { UserRepository } from './repositories/user.repository';
import { UserService } from './services/user.service';
import { USER_REPO, USER_SERVICE } from './user.constant';
const providers: Provider[] = [
  {
    provide: USER_SERVICE,
    useClass: UserService,
  },
  {
    provide: USER_REPO,
    useClass: UserRepository,
  },
];

@Module({
  imports: [SequelizeModule.forFeature([User]), forwardRef(() => AuthModule)],
  controllers: [UserController],
  providers: [...providers],
  exports: [...providers],
})
export class UserModule {}
