import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { User } from 'src/user/entities/user.entity';

export interface IUserRepository {
  createUser(createUserDto?: CreateUserDto): Promise<User>;
  findOneByEmail(email: string): Promise<User>;
  findById(id: string): Promise<User>;
  updateUser(id: string, updateUserDto: any): Promise<User>;
  deleteUser(id: string): Promise<void>;
}
