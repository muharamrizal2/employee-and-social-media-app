## Elastic app
To server data from elastic
## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.
Node 20.11.1 version

## Installation

```bash
$ yarn install
```
on employee directory

## Installation database
pre - 
make sure docker and docker-compose already installed
```bash
$ docker-compose up -d
```
```chmod +x create_index.sh ```  on terminal on directory dataset
```./create_index.sh       ``` execute on terminal (to run the bash script)

on dataset directory


## Running the app
# watch mode
 
  $ yarn run start dev

## Test
not finished

## End point documentations
LINK [https://documenter.getpostman.com/view/6168502/2sA3XMj3iH]


## change

```
{
    "settings": {
        "analysis": { -> This code purpose to split all text with ',' as separator
            "tokenizer": {
                "comma_tokenizer": {
                "type": "pattern",
                "pattern": ","
                }
            },
            "analyzer": {
                "comma_analyzer": {
                "type": "custom",
                "tokenizer": "comma_tokenizer"
                }
            }
        }
    },
    "mappings" : { -> several types change into keyword to make elastic can run aggregation by degfault since the value have constant value
        "employees" : {
            "properties" : {
                "FirstName" : { "type" : "text" },
                "LastName" : { "type" : "text" },
                "Designation" : { "type" : "keyword" }, // change into keyword
                "Salary" : { "type" : "integer" },
                "DateOfJoining" : { "type" : "date", "format": "yyyy-MM-dd" },
                "Address" : { "type" : "text" },
                "Gender" : { "type" : "keyword" }, // change into keyword
                "Age" : { "type" : "integer" },
                "MaritalStatus" : { "type" : "keyword" }, // change into keyword
                "Interests": { 
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword" // change into keyword
                        }
                    },
                    "analyzer": "comma_analyzer"
                }
            }
        }
    }
}
```