export interface AggregationResult<T> {
    value: T;
  }
  
  export interface Bucket {
    key: string | number;
    doc_count: number;
  }
  
  export interface Aggregations {
    avg_salary: AggregationResult<number>;
    min_salary: AggregationResult<number>;
    max_salary: AggregationResult<number>;
    age_distribution: {
      buckets: Bucket[];
    };
    gender_distribution: {
      buckets: Bucket[];
    };
    marital_status_distribution: {
      buckets: Bucket[];
    };
    date_of_joining_histogram: {
      buckets: {
        key_as_string: string;
        key: number;
        doc_count: number;
      }[];
    };
    top_interests: {
      buckets: Bucket[];
    };
    designation_distribution: {
      buckets: Bucket[];
    };
  }
  