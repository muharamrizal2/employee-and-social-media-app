import { Controller, Get } from '@nestjs/common';
import { ElasticService } from './elastic.service';

@Controller('metrics')
export class ElasticController {
  constructor(private readonly elasticService: ElasticService) {}

  @Get('count')
  async getCount() {
    return this.elasticService.getCount();
  }

  @Get('average-salary')
  async getAverageSalary() {
    return this.elasticService.getAverageSalary();
  }

  @Get('min-max-salary')
  async getMinMaxSalary() {
    return this.elasticService.getMinMaxSalary();
  }

  @Get('age-distribution')
  async getAgeDistribution() {
    return this.elasticService.getAgeDistribution();
  }

  @Get('gender-distribution')
  async getGenderDistribution() {
    return this.elasticService.getGenderDistribution();
  }

  @Get('marital-status-distribution')
  async getMaritalStatusDistribution() {
    return this.elasticService.getMaritalStatusDistribution();
  }

  @Get('date-of-joining-histogram')
  async getDateOfJoiningHistogram() {
    return this.elasticService.getDateOfJoiningHistogram();
  }

  @Get('top-interests')
  async getTopInterests() {
    return this.elasticService.getTopInterests();
  }

  @Get('designation-distribution')
  async getDesignationDistribution() {
    return this.elasticService.getDesignationDistribution();
  }
}
