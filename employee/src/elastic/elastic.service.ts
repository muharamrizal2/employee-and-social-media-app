import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { Aggregations } from './elastic.interface';

@Injectable()
export class ElasticService {
  constructor(private readonly elasticsearchService: ElasticsearchService) {}

  async getCount() {
    const result = await this.elasticsearchService.count({ index: 'companydatabase' });
    return result.body.count;
  }

  async getAverageSalary() {
    const { body } = await this.elasticsearchService.search({
      index: 'companydatabase',
      body: {
        aggs: {
          avg_salary: { avg: { field: 'Salary' } },
        },
      },
    });
    const aggregations = body.aggregations as Aggregations;
    return aggregations.avg_salary.value;
  }

  async getMinMaxSalary() {
    const { body } = await this.elasticsearchService.search({
      index: 'companydatabase',
      body: {
        aggs: {
          min_salary: { min: { field: 'Salary' } },
          max_salary: { max: { field: 'Salary' } },
        },
      },
    });
    const aggregations = body.aggregations as Aggregations;
    return {
      min: aggregations.min_salary.value,
      max: aggregations.max_salary.value,
    };
  }

  async getAgeDistribution() {
    const { body } = await this.elasticsearchService.search({
      index: 'companydatabase',
      body: {
        aggs: {
          age_distribution: {
            histogram: { field: 'Age', interval: 5 },
          },
        },
      },
    });
    const aggregations = body.aggregations as Aggregations;
    return aggregations.age_distribution.buckets;
  }

  async getGenderDistribution() {
    const result = await this.elasticsearchService.search({
      index: 'companydatabase',
      body: {
        aggs: {
          gender_distribution: {
            terms: {
              field: 'Gender'
            }
          }
        }
      }
    });
    const aggregations = result.body.aggregations as Aggregations;
    return aggregations.gender_distribution.buckets;
  }

  async getMaritalStatusDistribution() {
    const { body } = await this.elasticsearchService.search({
      index: 'companydatabase',
      body: {
        aggs: {
          marital_status_distribution: {
            terms: { field: 'MaritalStatus' },
          },
        },
      },
    });
    const aggregations = body.aggregations as Aggregations;
    return aggregations.marital_status_distribution.buckets;
  }

  async getDateOfJoiningHistogram() {
    const { body } = await this.elasticsearchService.search({
      index: 'companydatabase',
      body: {
        aggs: {
          date_of_joining_histogram: {
            date_histogram: { field: 'DateOfJoining', interval: 'year' },
          },
        },
      },
    });
    const aggregations = body.aggregations as Aggregations;
    return aggregations.date_of_joining_histogram.buckets;
  }

  async getTopInterests() {
    const result = await this.elasticsearchService.search({
      index: 'companydatabase',
      body: {
        aggs: {
          top_interests: {
            terms: { field: 'Interests.keyword' },
          },
        },
      },
    });
    const aggregations = result.body.aggregations;
    return aggregations.top_interests.buckets;
  }

  async getDesignationDistribution() {
    const { body } = await this.elasticsearchService.search({
      index: 'companydatabase',
      body: {
        aggs: {
          designation_distribution: {
            terms: { field: 'Designation' },
          },
        },
      },
    });
    const aggregations = body.aggregations as Aggregations;
    return aggregations.designation_distribution.buckets;
  }
}
