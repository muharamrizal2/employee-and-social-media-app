#!/bin/bash

curl -XPUT 'http://127.0.0.1:9200/companydatabase?pretty' \
-H 'Content-Type: application/json' \
-d '{
    "settings": {
        "analysis": {
            "tokenizer": {
                "comma_tokenizer": {
                "type": "pattern",
                "pattern": ","
                }
            },
            "analyzer": {
                "comma_analyzer": {
                "type": "custom",
                "tokenizer": "comma_tokenizer"
                }
            }
        }
    },
    "mappings" : {
        "employees" : {
            "properties" : {
                "FirstName" : { "type" : "text" },
                "LastName" : { "type" : "text" },
                "Designation" : { "type" : "keyword" },
                "Salary" : { "type" : "integer" },
                "DateOfJoining" : { "type" : "date", "format": "yyyy-MM-dd" },
                "Address" : { "type" : "text" },
                "Gender" : { "type" : "keyword" },
                "Age" : { "type" : "integer" },
                "MaritalStatus" : { "type" : "keyword" },
                "Interests": { 
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword"
                        }
                    },
                    "analyzer": "comma_analyzer"
                }
            }
        }
    }
}'

curl -XPOST "http://127.0.0.1:9200/companydatabase/_bulk" \
--header "Content-Type: application/json" \
--data-binary "@./Employees50K.json"
